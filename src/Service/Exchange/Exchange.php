<?php

namespace App\Service\Exchange;

use Psr\SimpleCache\CacheInterface;

/**
 * Class Exchange
 * @package App\Exchange
 */
class Exchange
{
    private $exchange;
    /**
     * @var CacheInterface
     */
    private $cache;

    private $cachePrefix = 'exchange';

    /**
     * Exchange constructor.
     * @param ExchangeInterface $exchange
     * @param CacheInterface $cache
     */
    public function __construct(ExchangeInterface $exchange, CacheInterface $cache)
    {
        $this->exchange = $exchange;
        $this->cache = $cache;

    }

    /**
     * @return Rates
     */
    public function getLatestRates()
    {
        $cacheKey = $this->getCacheKey('latest');
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }
        $latestRates = $this->exchange->latestRates();
        $this->cache->set($cacheKey, $latestRates, \DateInterval::createFromDateString('5 minutes'));
        return $latestRates;
    }

    /**
     * @return Currencies
     */
    public function getCurrencies()
    {
        $cacheKey = $this->getCacheKey('currencies');
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }
        $currencies = $this->exchange->currencies();
        $this->cache->set($cacheKey, $currencies);
        return $currencies;
    }

    /**
     * @return ExchangeInterface
     */
    public function getExchange()
    {
        return $this->exchange;
    }

    /**
     * @param $endpoint
     * @return string
     */
    private function getCacheKey($endpoint)
    {
        $cacheKey = $this->cachePrefix . '.' . $this->exchange->getName() . '.' . $endpoint;
        return $cacheKey;
    }
}
