<?php

namespace App\Service\Exchange;


use App\Service\Exchange\Exception\RateException;

/**
 * Class Rates
 * @package App\Exchange
 */
class Rates
{
    /**
     * @var string
     */
    private $base;

    /**
     * @var array
     */
    private $rates = [];

    /**
     * @var int
     */
    private $timestamp;

    /**
     * Rates constructor.
     * @param $base
     * @param $timestamp
     */
    public function __construct($base, $timestamp)
    {
        $this->base = $base;
        $this->timestamp = $timestamp;
    }

    /**
     * @param $currency
     * @param $rate
     */
    public function addRate($currency, $rate)
    {
        $this->rates[$currency] = new Rate($currency, $rate);
    }

    /**
     * @param $toCurrency
     * @param $amount
     * @return float|int
     */
    public function convertFromBaseCurrency($toCurrency, $amount)
    {
        return $amount * $this->getRate($toCurrency);
    }

    /**
     * @param $fromCurrency
     * @param $amount
     * @return float|int
     */
    public function convertToBaseCurrency($fromCurrency, $amount)
    {
        return  $amount / $this->getRate($fromCurrency);
    }

    /**
     * @param $toCurrency
     * @param $fromCurrency
     * @param $amount
     * @return float|int
     */
    public function convert($fromCurrency, $toCurrency, $amount)
    {
        if ($amount == 0) {
            return 0;
        }
        return  $this->convertToBaseCurrency($fromCurrency, $amount) * $this->getRate($toCurrency);
    }

    /**
     * @param $currency
     * @return Rate
     * @throws RateException
     */
    public function getRate($currency)
    {
        if (!isset($this->rates[$currency])) {
            throw new RateException('There is no ' . $currency . ' rate');
        }

        return $this->rates[$currency]->getRate();
    }

    /**
     * @return mixed
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @return array
     */
    public function getRates(): array
    {
        return $this->rates;
    }
}
