<?php

namespace App\Service\Exchange;

/**
 * Interface ExchangeInterface
 * @package App\Exchange
 */
interface ExchangeInterface
{
    /**
     * @return Rates
     */
    public function latestRates();

    /**
     * @return Currencies
     */
    public function currencies();

    /**
     * @return string
     */
    public function getName();
}
