<?php

namespace App\Service\Exchange;

/**
 * Class Currencies
 * @package App\Exchange
 */
class Currencies
{
    private $currencies = [];

    /**
     * @param $currency
     * @param $name
     */
    public function add($currency, $name)
    {
        $this->currencies[$currency] = new Currency($currency, $name);
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->currencies;
    }

    /**
     * @param $currency
     * @return mixed
     */
    public function getNameByCurrency($currency)
    {
        return $this->currencies[$currency]->getName();
    }
}
