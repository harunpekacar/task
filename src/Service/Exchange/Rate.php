<?php

namespace App\Service\Exchange;

/**
 * Class Rate
 * @package App\Exchange
 */
class Rate
{
    /**
     * @var string
     */
    private $currency;

    /**
     * @var float
     */
    private $rate;

    /**
     * Rate constructor.
     * @param $currency
     * @param $rate
     */
    public function __construct($currency, $rate)
    {
        $this->currency = $currency;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Rate
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     * @return Rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this;
    }


}
