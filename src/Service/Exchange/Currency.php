<?php

namespace App\Service\Exchange;

/**
 * Class Currency
 * @package App\Exchange
 */
class Currency
{
    private $currency;
    private $name;

    /**
     * Currency constructor.
     * @param $currency
     * @param $name
     */
    public function __construct($currency, $name)
    {
        $this->currency = $currency;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}
