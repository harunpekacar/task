<?php

namespace App\Service\Exchange\OpenExchange;

use App\Service\Exchange\Currencies;
use App\Service\Exchange\ExchangeInterface;
use App\Service\Exchange\Rates;
use GuzzleHttp\Client;

/**
 * Class OpenExchange
 * @package App\Exchange\OpenExchange
 */
class OpenExchange implements ExchangeInterface
{
    /**
     * @var string
     */
    private $name = 'openexchange';
    /**
     * @var string
     */
    private $apiUrl = 'https://openexchangerates.org/api';
    /**
     * @var string
     */
    private $appId = null;

    /**
     * OpenExchange constructor.
     * @param $appId
     */
    public function __construct($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return Rates
     */
    public function latestRates(): Rates
    {
        $latestRates = $this->get('latest.json');
        $baseCurrency = $latestRates->base;
        $timestamp = $latestRates->timestamp;
        $rates = new Rates($baseCurrency, $timestamp);

        foreach ($latestRates->rates as $key => $rate) {
            $rates->addRate($key, $rate);
        }
        return $rates;
    }

    /**
     * @return Currencies
     */
    public function currencies()
    {
        $currenciesFromApi = $this->get('currencies.json');
        $currencies = new Currencies();
        foreach ($currenciesFromApi as $currency => $name) {
            $currencies->add($currency, $name);
        }
        return $currencies;
    }


    /**
     * @param $endpoint
     * @param array $extra
     * @return mixed
     */
    private function get($endpoint, $extra = [])
    {
        $extra['app_id'] = $this->appId;
        $query = http_build_query($extra);
        $url = $this->apiUrl . '/' . $endpoint . '?' . $query;
        $client = new Client();
        $response = $client->get($url);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
