<?php

namespace App\Service\CsvImporter;

use App\Serializer\Normalizer\CsvNormalizer;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class CsvImporter
 * @package App\Service
 */
class CsvImporter
{
    /**
     * @var UploadedFile
     */
    private $file;
    /**
     * @var string
     */
    private $class;

    /**
     * CsvImporter constructor.
     * @param UploadedFile $file
     * @param $class
     */
    public function __construct(UploadedFile $file, $class)
    {
        $this->file = $file;
        $this->class = $class;
    }

    /**
     * @return object
     */
    public function getData()
    {
        $class = $this->class;
        $serializer = new Serializer([new ArrayDenormalizer(), new ObjectNormalizer()], [new CsvEncoder()]);
        $data = $serializer->deserialize($this->getContent(), sprintf('%s[]', $class), 'csv');
        return $data;
    }

    /**
     * @return bool|string
     */
    private function getContent()
    {
        return file_get_contents($this->file);
    }
}
