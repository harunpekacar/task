<?php

namespace App\Command;

use App\Service\Exchange\Exchange;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class LastExchangeRatesCommand
 * @package App\Command
 */
class LastExchangeRatesCommand extends Command
{
    protected static $defaultName = 'exchange:lastest';
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var Exchange
     */
    private $exchange;

    /**
     * LastExchangeRatesCommand constructor.
     * @param CacheInterface $cache
     * @param Exchange $exchange
     */
    public function __construct(CacheInterface $cache, Exchange $exchange)
    {
        parent::__construct();
        $this->exchange = $exchange;
        $this->cache = $cache;
    }

    /**
     * configure
     */
    protected function configure()
    {
        $this->setDescription('Latest Exchange Rates')
            ->addArgument('rate', InputArgument::REQUIRED, 'Currency Rate');

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $rate = floatval($input->getArgument('rate'));
        $lastestRates = $this->exchange->getLatestRates();
        $io->text('Base Currency is '.$lastestRates->getBase());
        $io->text('Amount is convertiong to '. 'TRY');
        $io->text($lastestRates->convertFromBaseCurrency('TRY', $rate));
        $io->text($lastestRates->convertToBaseCurrency('TRY', $rate));
    }
}
