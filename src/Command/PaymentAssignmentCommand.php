<?php

namespace App\Command;

use App\Entity\Loan;
use App\Entity\LoanPayment;
use App\Entity\Payment;
use App\Repository\LoanPaymentRepository;
use App\Repository\LoanRepository;
use App\Repository\PaymentRepository;
use App\Service\Exchange\Exchange;
use Doctrine\ORM\EntityManagerInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PaymentAssignmentCommand
 * @package App\Command
 */
class PaymentAssignmentCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'payment:assignment';

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Exchange
     */
    protected $exchange;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * PaymentAssignmentCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param Exchange $exchange
     * @param CacheInterface $cache
     */
    public function __construct(EntityManagerInterface $entityManager, Exchange $exchange, CacheInterface $cache)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->exchange = $exchange;
        $this->cache = $cache;
    }

    /**
     * configure
     */
    protected function configure()
    {
        $this->setDescription('Payments assignment process');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        while (true) {
//            if($this->cache->get('paymentAssignment', false))  {
                /** @var PaymentRepository $paymentRepository */
                $paymentRepository = $this->entityManager->getRepository(Payment::class);
                /** @var LoanRepository $loanRepository */
                $loanRepository = $this->entityManager->getRepository(Loan::class);
                /** @var LoanPaymentRepository $loanRepository */
                $loanPaymentRepository = $this->entityManager->getRepository(LoanPayment::class);
                /** @var Payment[] $payments */
                $payments = $paymentRepository->getNotAssignedPayments();
                foreach ($payments as $payment) {
                    $paymentInfo = $payment->getPaymentInfo();
                    /** @var Loan $loan */
                    $loan = $loanRepository->findOneByLoanNumber($paymentInfo);

                    if (!empty($loan) && !$loanPaymentRepository->isPaid($payment->getId(), $loan->getId())) {
                        $totalPaidAmount = $loanPaymentRepository->getTotalPaidAmount($loan->getId());
                        $remainingTotalAmount = $loan->getAmount() - $totalPaidAmount;
                        $loanPayment = new LoanPayment();
                        $exchangedAmount = $this->exchange->getExchange()->latestRates()->convert($payment->getCurrency(), $loan->getCurrency(), $payment->getAmount());
                        $remainingAmount = $remainingTotalAmount - $exchangedAmount;
                        $loanPayment->setLoan($loan);
                        $loanPayment->setPayment($payment);
                        $loanPayment->setAmount($exchangedAmount);
                        $loanPayment->setRemainingAmount($remainingAmount);
                        $loanPayment->setCurrency($loan->getCurrency());
                        $this->entityManager->persist($loanPayment);
                        if ($remainingAmount == 0) {
                            $payment->setStatus(Payment::ASSIGNED);
                            $loan->setStatus(Loan::PAID);
                        } else {
                            $payment->setStatus(Payment::PARTIALLY_ASSIGNED);
                        }
                    }
                    $this->entityManager->flush();
                }

//                $io->text('Payment assigned on ' . date('Y-m-d H:i:s'));
//                $this->cache->delete('paymentAssignment');
            }
            sleep(10);
//        }
    }
}
