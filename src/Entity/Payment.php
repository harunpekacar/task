<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 * @ORM\Table(name="payments",indexes={@ORM\Index(name="payment_number_idx", columns={"payment_number"}),@ORM\Index(name="payment_info_idx", columns={"payment_info"})})
 */
class Payment
{
    const NOT_ASSIGNED = 1;
    const PARTIALLY_ASSIGNED = 2;
    const ASSIGNED = 3;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $payment_number;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=3)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $payment_info;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $currency;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * Payments constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @param mixed $id
     * @return Payment
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPaymentNumber()
    {
        return $this->payment_number;
    }

    /**
     * @param mixed $payment_number
     * @return Payment
     */
    public function setPaymentNumber($payment_number)
    {
        $this->payment_number = $payment_number;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $amount
     * @return Payment
     */
    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentInfo()
    {
        return $this->payment_info;
    }

    /**
     * @param mixed $payment_info
     * @return Payment
     */
    public function setPaymentInfo($payment_info)
    {
        $this->payment_info = $payment_info;
        return $this;
    }


    /**
     * @return null|string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Payment
     */
    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Payment
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    /**
     * @param \DateTimeInterface $created_at
     * @return Payment
     */
    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    /**
     * @param \DateTimeInterface|null $updated_at
     * @return Payment
     */
    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime());
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime());
        }
    }

    /**
     * @return string
     */
    public function getStatusText()
    {
        $status = '';
        if ($this->status == self::NOT_ASSIGNED) {
            $status = '<span class="badge badge-success">Not Assigned</span>';
        } elseif ($this->status == self::PARTIALLY_ASSIGNED) {
            $status = '<span class="badge badge-success">Partially Assigned</span>';
        } elseif ($this->status == self::ASSIGNED) {
            $status = '<span class="badge badge-danger">Assigned</span>';
        }
        return $status;
    }
}
