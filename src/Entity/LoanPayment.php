<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LoanPaymentRepository")
 * @ORM\Table(name="loan_payments",indexes={@ORM\Index(name="payment_idx", columns={"payment_id"}),@ORM\Index(name="loan_idx", columns={"loan_id"})})
 */
class LoanPayment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Payments constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @ORM\Column(type="integer")
     */
    private $loan_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $payment_id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=3)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $currency;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=3, nullable=true)
     */
    private $remaining_amount;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Loan")
     * @ORM\JoinColumn(name="loan_id", referencedColumnName="id", nullable=false)
     */
    private $loan;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Payment")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id", nullable=false)
     */
    private $payment;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getLoanId(): int
    {
        return $this->loan_id;
    }

    /**
     * @param int $loan_id
     * @return LoanPayment
     */
    public function setLoanId(int $loan_id): self
    {
        $this->loan_id = $loan_id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPaymentId(): ?int
    {
        return $this->payment_id;
    }

    /**
     * @param int $payment_id
     * @return LoanPayment
     */
    public function setPaymentId(int $payment_id): self
    {
        $this->payment_id = $payment_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param $amount
     * @return LoanPayment
     */
    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRemainingAmount()
    {
        return $this->remaining_amount;
    }

    /**
     * @param $remaining_amount
     * @return LoanPayment
     */
    public function setRemainingAmount($remaining_amount): self
    {
        $this->remaining_amount = $remaining_amount;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    /**
     * @param \DateTimeInterface $created_at
     * @return LoanPayment
     */
    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    /**
     * @param \DateTimeInterface|null $updated_at
     * @return LoanPayment
     */
    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime());
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime());
        }
    }

    /**
     * @return Loan
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $loan
     * @return LoanPayment
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;
        return $this;
    }

    /**
     * @param mixed $payment
     * @return LoanPayment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
        return $this;
    }



    /**
     * @return null|string
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return LoanPayment
     */
    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
