<?php

namespace App\Repository;

use App\Entity\LoanPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LoanPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method LoanPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method LoanPayment[]    findAll()
 * @method LoanPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoanPaymentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LoanPayment::class);
    }

    /**
     * @param $paymentId
     * @param $loanId
     * @return mixed
     */
    public function isPaid($paymentId, $loanId)
    {
        $payment = $this->createQueryBuilder('lp')
            ->andWhere('lp.loan_id = :loan_id')
            ->andWhere('lp.payment_id = :payment_id')
            ->setParameter('loan_id', $loanId)
            ->setParameter('payment_id', $paymentId)
            ->getQuery()
            ->getOneOrNullResult();
        return empty($payment) ? false : true;
    }

    /**
     * @param $loanId
     * @return float
     */
    public function getTotalPaidAmount($loanId)
    {
        $totalAmount = $this->createQueryBuilder('lp')
            ->select('SUM(lp.amount) as total')
            ->andWhere('lp.loan_id = :loan_id')
            ->setParameter('loan_id', $loanId)
            ->getQuery()
            ->getOneOrNullResult();
        return $totalAmount['total'] == null ? 0 : $totalAmount['total'];
    }

}
