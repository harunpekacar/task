<?php

namespace App\Repository;

use App\Entity\Loan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Loan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Loan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Loan[]    findAll()
 * @method Loan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoanRepository extends ServiceEntityRepository
{
    /**
     * LoanRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Loan::class);
    }

    /**
     * @param $loanNumber
     * @return Loan|null
     */
    public function findOneByLoanNumber($loanNumber)
    {
        return $this->createQueryBuilder('l')->andWhere('l.loan_number = :loan_number')
            ->setParameter('loan_number', $loanNumber)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
