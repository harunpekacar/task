<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Repository\PaymentRepository;
use App\Service\CsvImporter\CsvImporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PaymentController
 * @package App\Controller
 */
class PaymentController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EntityManagerInterface $entityManager)
    {
        $repository = $entityManager->getRepository(Payment::class);
        $payments = $repository->findAll();
        return $this->render('payment/index.html.twig', compact('payments'));
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function upload(Request $request, EntityManagerInterface $entityManager)
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        if($file instanceof UploadedFile && $file->getClientOriginalExtension() != 'csv'){
            $this->addFlash(
                'error',
                'Your file is not csv'
            );
            return $this->redirectToRoute('payments');
        }
        $csv = new CsvImporter($file, Payment::class);
        /** @var Payment[] $payments */
        $payments = $csv->getData();
        foreach ($payments as $payment) {
            $paymentNumber = $payment->getPaymentNumber();
            /** @var PaymentRepository $repository */
            $repository = $entityManager->getRepository(Payment::class);
            /** @var Payment $paymentFromDb */
            $paymentFromDb = $repository->findOneByPaymentNumber($paymentNumber);
            if (empty($paymentFromDb)) {
                $currency = trim($payment->getCurrency());
                $paymentNumber = trim($payment->getPaymentNumber());
                $paymentInfo = trim($payment->getPaymentInfo());
                $payment->setCurrency($currency);
                $payment->setPaymentNumber($paymentNumber);
                $payment->setPaymentInfo($paymentInfo);
                $payment->setStatus(Payment::NOT_ASSIGNED);
                $entityManager->persist($payment);
            }
        }
        $entityManager->flush();
        $this->addFlash(
            'success',
            'Your payments were saved!'
        );
//        $this->cache->set('paymentAssignment', true);
        return $this->redirectToRoute('payments');
    }
}
