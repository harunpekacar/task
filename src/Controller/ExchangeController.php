<?php

namespace App\Controller;

use App\Service\Exchange\Exchange;
use App\Service\Exchange\OpenExchange\OpenExchange;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class ExchangeController
 * @package App\Controller
 */
class ExchangeController extends BaseController
{

    /**
     * @param Exchange $exchange
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Exchange $exchange)
    {
        $currencies = $exchange->getCurrencies()->get();
        return $this->render('exchange/index.html.twig', compact('currencies'));
    }

    /**
     * @param Request $request
     * @param Exchange $exchange
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function exchange(Request $request, Exchange $exchange)
    {
        try {
            $to = $request->get('to', '');
            $from = $request->get('from', '');
            $amount = floatval($request->get('amount', 0));
            $currencies = $exchange->getCurrencies();
            $rates = $exchange->getLatestRates();
            $convertedAmount = $rates->convert($from, $to, $amount);
            $response = [
                'success' => true,
                'to' => [
                    'currency' => $to,
                    'name' => $currencies->getNameByCurrency($to),
                    'rate' => $rates->convert($to, $from, 1),
                ],
                'from' => [
                    'currency' => $from,
                    'name' => $currencies->getNameByCurrency($from),
                    'rate' => $rates->convert($from, $to, 1)
                ],
                'amount' => round($amount, 3),
                'result' => round($convertedAmount, 3)
            ];
        } catch (\Exception $ex){
            $response = [
                'success' => false
            ];
        }
        return $this->json($response);
    }
}
