<?php

namespace App\Controller;

use App\Entity\LoanPayment;
use App\Repository\LoanPaymentRepository;
use Doctrine\ORM\EntityManagerInterface;

class TransactionController extends BaseController
{
    /**
     * @param EntityManagerInterface $entityManager
     * @return mixed
     */
    public function index(EntityManagerInterface $entityManager)
    {
        /** @var LoanPaymentRepository $repository */
        $repository = $entityManager->getRepository(LoanPayment::class);
        /** @var LoanPayment[] $transactions */
        $transactions = $repository->findAll();
        return $this->render('transactions/index.html.twig', compact('transactions'));
    }
}
