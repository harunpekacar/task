<?php

namespace App\Controller;

use App\Entity\Loan;
use App\Repository\LoanRepository;
use App\Repository\PaymentRepository;
use App\Service\Csv;
use App\Service\CsvImporter\CsvImporter;
use App\Service\LoanCsv;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoanController
 * @package App\Controller
 */
class LoanController extends BaseController
{
    /**
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EntityManagerInterface $entityManager)
    {
        $repository = $entityManager->getRepository(Loan::class);
        $loans = $repository->findAll();
        return $this->render('loan/index.html.twig', compact('loans'));
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function upload(Request $request, EntityManagerInterface $entityManager)
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        if($file instanceof UploadedFile && $file->getClientOriginalExtension() !=  'csv'){
            $this->addFlash(
                'error',
                'Your file is not csv'
            );
            return $this->redirectToRoute('payments');
        }
        $csv = new CsvImporter($file, Loan::class);
        /** @var Loan[] $loans */
        $loans = $csv->getData();
        foreach ($loans as $loan) {
            $loanNumber = $loan->getLoanNumber();
            /** @var LoanRepository $repository */
            $repository = $entityManager->getRepository(Loan::class);
            /** @var Loan $loanFromDb */
            $loanFromDb = $repository->findOneByLoanNumber($loanNumber);
            if(empty($loanFromDb)) {
                $currency = trim($loan->getCurrency());
                $loanNumber = trim($loan->getLoanNumber());
                $loan->setCurrency($currency);
                $loan->setLoanNumber($loanNumber);
                $loan->setStatus(Loan::ACTIVE);
                $entityManager->persist($loan);
            }
        }
        $entityManager->flush();
        $this->addFlash(
            'success',
            'Your loans were saved!'
        );
        return $this->redirectToRoute('loans');
    }
}
