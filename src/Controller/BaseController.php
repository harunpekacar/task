<?php

namespace App\Controller;


use Psr\SimpleCache\CacheInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{
    protected $cache;
    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }
}
