<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190327003824 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE loan_payments ADD CONSTRAINT FK_32946199CE73868F FOREIGN KEY (loan_id) REFERENCES loans (id)');
        $this->addSql('ALTER TABLE loan_payments ADD CONSTRAINT FK_329461994C3A3BB FOREIGN KEY (payment_id) REFERENCES payments (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_32946199CE73868F ON loan_payments (loan_id, payment_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE loan_payments DROP FOREIGN KEY FK_32946199CE73868F');
        $this->addSql('ALTER TABLE loan_payments DROP FOREIGN KEY FK_329461994C3A3BB');
        $this->addSql('DROP INDEX UNIQ_32946199CE73868F ON loan_payments');
    }
}
