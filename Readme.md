#Task
## Installation
Run the following command:

```
docker-compose up -d
docker exec -it task_php composer update
docker exec -it task_php /usr/local/bin/php bin/console doctrine:migrations:migrate
```

And it will start all those containers in the right order. Once started, you can point your browser at http://localhost on 80 port. If your 80 port is using from another program, you can change your port on docker-compose.yml in nginx.  

I developed with symfony 4. This application run in docker containers. Docker containers is **php 7.2**, **nginx** and **mysql**.

I used  bootstrap and jquery(full version). The project is not including files. I wanted to use them quickly. I used cdn links .

1. 
    First instruction is service container. You can see in config/services.yml file.
    
    /service/ExchangeInterface.php
    
    I created Exchange interface, that has latestRates, currencies and getName method signatures.
    
    OpenExchange implement this interface. Other exchange api services must implement this interface and be added service.yml.
    
    I wrote entities. Entities are rate and currency, currencies. They are using returning values from service. Currency conversion is in rates class. I wrote 3 method for it.
     

    > public function convert($fromCurrency, $toCurrency, $amount);
    
    > It use to convert from x currency to y currecy in rates. If one of those currencies dont have in rates, it throw RateException. I'm using this function to convert currencies. 
    
    > public function convertToBaseCurrency($fromCurrency, $amount);
    
    > It's using convert currency to base currency
    
    > public function convertFromBaseCurrency($toCurrency, $amount);
    
    > It use convert currency from base currency
    
    Currency conversation controller is ExchangeController. It is has two method. One of them is for view, other is for xhr request to convert conversation it could have converted all currencies in browser. I wrote xhr request firstly.
    
    I saw that now(27 March 23:55).
    
    > Create simple calculator form to convert from provided currency to USD and from USD to provided currency
    
    Sorry i cannot change now. after i read, if you want, i can write it.
    
    I have command for it. if you want to look, you can use the following command:
        
    > docker exec -it task_php /usr/local/bin/php bin/console exchange:lastest <rate>
    
    I used file cache to store api entities. For using redis cache.yml must change.
    
2. http://localhost/loan -> you can see loans and upload csv. Csv file won't move from tmp. it read from tmp. I wrote simple csv import. It fill given entity and return array of that entity.
    
    http://localhost/payment -> you can see payments and upload csv.
    
    *Bug: I didn't write to check csv columns. CsvImporter fill entity member variables for csv columns. Even csv file has not got loan_number, data will save in loan table*
    
4. I wrote command for it. That command run all time. It has endless while and sleep. (PaymentAssignmentCommand). Php-fpm container has supervisor. Supervisor control that process and php-fpm.
    
    It's simple assignment process. It takes all active payment after that, takes loan of payment, and then adds loan_payments table and changes status. It's transaction table. Transaction table in the database which links payments and loans.
    
    It can be reverse. Takes loan, and takes payments of loan and same order... 


###Solutions

I wrote three of them in payment assignment command.

1. Partial payments - payment covers only some part of loan

    I mentioned before, every transaction stores in loan_payments table. It has amount and remaining amount. I store the remaining about of the overall debt. every payment I change the remaining amount of the loan depending on the payment.
    I used partially assigned status in it.  

2. Overpayments - Payment is larger than loan amount

    they are being in the same way as partial payments. Only the remaining amount will be negative. Because loan_payments table is depend on loan. every currency and amount is from loan.
    Maybe it may add payment table again to process. 

3.  Loans and payments can be stored in other currencies

    When loans and payments are stored in different currencies, I convert the payment into the loan currency and then execute the payment.

4. Assume that system now has clients and invoices. Payment info can have client number, invoice number, loan number. How would the assignment process change and what might be the issues, if we have priorities in which order assignment should be processed, e.g., invoices should be checked first, loans second etc.

    For this case, i would modify loan_payments table to include transaction type, which could either loan or an invoice etc. I would change table of loan_payments to transactions. And then I execute the transactions the same way. I would write code to parse payment info, to determine how to process them. so in this case, I would process the invoice, calculate the remaining amount of the payment on transaction table after deducting the invoice amount from it, and then use the remaining for the loan.  
